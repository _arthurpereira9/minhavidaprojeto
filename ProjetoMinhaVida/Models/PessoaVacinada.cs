﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoMinhaVida.Models
{
    public class PessoaVacinada
    {        
        public int IdPessoaVacinada { get; set; }
        [Required]
        public int IdPessoa { get; set; }
        [Required]
        public int IdVacina { get; set; }
        public DateTime DataAplicacao { get; set; }
        public DateTime DataVencimento { get; set; }
    }
}
