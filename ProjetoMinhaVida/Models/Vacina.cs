﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoMinhaVida.Models
{
    public class Vacina
    {
        public int IdVacina { get; set; }
        [Required]
        public string Nome { get; set; }
    }
}
