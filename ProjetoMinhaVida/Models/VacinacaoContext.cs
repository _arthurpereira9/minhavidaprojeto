﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoMinhaVida.Models
{
    public class VacinacaoContext : DbContext
    {
        public VacinacaoContext(DbContextOptions<VacinacaoContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pessoa>()
                   .HasKey(p => p.IdPessoa);
            modelBuilder.Entity<Vacina>()
                .HasKey(p => p.IdVacina);
            modelBuilder.Entity<PessoaVacinada>()
                .HasKey(p => p.IdPessoaVacinada);
        }

        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Vacina> Vacinas { get; set; }
        public DbSet<PessoaVacinada> PessoasVacinadas { get; set; }
    }
}
